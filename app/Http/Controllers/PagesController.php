<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class PagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title = 'welcome to AsignacionTribunales!';
        //return view('pages.index', compact('title'));
        return view('pages.index');
    }

    public function about()
    {
        $title = 'About Us';
        //return view('pages.about');
        return view('pages.about')->with('title', $title);
    }
    public function login()
    {
    	return view('pages.login');
    }

    
}
