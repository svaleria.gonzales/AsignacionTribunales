@extends('layouts.Admin')
@section('content')
  <div class="container">
    <div class="col-sm-10" style="width: 600px; margin-left: 250px; margin-top: 50px;">
      <div class="jumbotron">
        <div class="form-group" style="margin-top: -50px">
          <h2 style="margin-left: 80px;">Edicion de administrador</h2>
          <img src="{{asset('imagenes/adm.png')}}" style="margin-left: 200px;">
        </div>

        <hr>
        {!! Form::open(['route' => ['admin.admin.update', $admin],'method'=>'PUT'],['style'=>'margin-left: 50px']) !!}
          <div class="form-group input-group">
            <span class="input-group-addon">
              <span class="glyphicon glyphicon-user"></span>
            </span>
            {!! Form::text('nombre',$admin->nombre,['class'=>'form-control','placeholder'=>'Ingrese su nombre','required']) !!}
          </div>
          <div class="form-group input-group">
            <span class="input-group-addon">
              <span class="glyphicon glyphicon-user"></span>
            </span>
            {!! Form::text('apellido',$admin->apellido,['class'=>'form-control','placeholder'=>'Ingrese su apellido','required']) !!}
          </div>
          <div class="form-group input-group">
            <span class="input-group-addon">
              <span class="glyphicon glyphicon-envelope"></span>
            </span>
            {!! Form::email('email',$admin->email,['class'=>'form-control','placeholder'=>'Ingrese su correo electronico','required']) !!}
          </div>
          <div class="form-group">
            {!! Form::submit('Modificar',['class'=>'btn btn-primary', 'style'=>'width: 450px']) !!}
          </div>
        {!! Form::close() !!}
      </div>
    </div>
  </div>
@endsection
@section('title')
  Admin-Editar-Admin
@endsection


@section('content-header')
  <section class="content-header">
      <h1>
        Zona de administracion
        <small>Editar Administrador</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="prueba"><i class="fa fa-user-circle-o"></i> Admin</a></li>
        <li><a href="prueba">Editar administrador</a></li>
      </ol>
    </section>
@endsection