<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Student;

class TestController extends Controller
{
    public function view($id){
        $student = Student::find($id);
        $student->project;
        return view('pruebas.index',['student'=>$student]);
    }
}
