
<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" type="text/css" href="{{asset('assets/css/bootstrap.css')}}">
	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
</head>
<body>
	<table class="table table-striped">
		<thead>
			<th>ID</th>
			<th>nombre</th>
			<th>apellido</th>
			<th>email</th>
			<th>carrera</th>
			<th>modalidad de titulacion</th>
		</thead>
		<tbody>
			@foreach($students as $student)
			<tr>
				<td>{{$student->id}}</td>
				<td>{{$student->name}}</td>
				<td>{{$student->lastname}}</td>
				<td>{{$student->email}}</td>
				<td>{{$student->career}}</td>
				<td>{{$student->degree_modality}}</td>
			</tr>
			@endforeach
		</tbody>
		{!! $students->render() !!}
	</table>
</body>
</html>