@extends('layouts.app')
@section('content')
<div class="container-fluid">
  <div class="row content">
    <div class="col-sm-2 sidenav">
      @yield('tituloVista')
      <hr>
      <ul class="nav nav-pills nav-stacked">
      	@yield('contenidoBIzq')
      </ul><br>
    </div>
    <div class="col-sm-10">
    	@yield('contenidoCuerpo')
    </div>
  </div>
</div>
@endsection