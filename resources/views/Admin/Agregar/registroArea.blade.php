@extends('layouts.Admin')
@section('content')

	@if(count($errors)>0)
	<div class="alert alert-danger" role="alert">
		<ul>
			@foreach($errors->all() as $error)
				<li>{{$error}}</li>
			@endforeach
		</ul>
	</div>
	@endif

	<div class="container">
		<div class="col-sm-10" style="width: 600px; margin-left: 250px; margin-top: 50px;">
			<div class="jumbotron">
				<div class="form-group" style="margin-top: -50px">
					<h2 style="margin-left: 130px;">Registro Area</h2>
					<img src="{{asset('imagenes/adm.png')}}" style="margin-left: 200px;">
				</div>

				<hr>
				{!! Form::open(['route' => 'area.area.store','method'=>'POST'],['style'=>'margin-left: 50px']) !!}
					<div class="form-group input-group">
						<span class="input-group-addon">
							<span class="glyphicon glyphicon-user"></span>
						</span>
						{!! Form::text('nombre',null,['class'=>'form-control','placeholder'=>'Nombre del area','required']) !!}
					</div>
					<div class="form-group input-group">
						<span class="input-group-addon">
							<span class="glyphicon glyphicon-user"></span>
						</span>
						{!! Form::text('descripcion',null,['class'=>'form-control','placeholder'=>'Descripcion del area']) !!}
					</div>
					<div class="form-group">
						{!! Form::submit('Registrar',['class'=>'btn btn-primary', 'style'=>'width: 450px']) !!}
					</div>

				{!! Form::close() !!}
			</div>
		</div>
	</div>
@endsection
@section('title')
	Admin-Registro-Admin
@endsection


@section('content-header')
	<section class="content-header">
      <h1>
        Zona de administracion
        <small>Registro Administrador</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="prueba"><i class="fa fa-user-circle-o"></i> Admin</a></li>
        <li><a href="prueba">Registro administrador</a></li>
      </ol>
    </section>
@endsection