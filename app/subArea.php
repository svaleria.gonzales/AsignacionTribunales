<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class subArea extends Model
{
    protected $table = "subareas";
    protected $fillable = ['nombre','area_id'];
    public function area(){
    	return $this->belongsTo('App\Area');
    }
}
