@extends('layouts.Admin')
@section('content')
	<div class="container">
		<div class="col-sm-10" style="width: 600px; margin-left: 250px; margin-top: 50px;">
			<div class="jumbotron">
				<div class="form-group" style="margin-top: -50px">
					<h2 style="margin-left: 100px;">Editar Estudiante</h2>
					<img src="{{asset('imagenes/estudiante.png')}}" style="margin-left: 200px;">
				</div>

				<hr>

				{!! Form::open(['route' => ['proyecto.proyecto.update',$proyecto],'method'=>'PUT'],['style'=>'margin-left: 50px']) !!}
					<div class="form-group input-group">
						<span class="input-group-addon">
							<span class="glyphicon glyphicon-user"></span>
						</span>
						{!! Form::text('nombrePostulante',$proyecto->nombrePostulante,['class'=>'form-control','placeholder'=>'Nombre del postulante','required']) !!}
					</div>
					<div class="form-group input-group">
						<span class="input-group-addon">
							<span class="glyphicon glyphicon-user"></span>
						</span>
						{!! Form::text('apellidoPPostulante',$proyecto->apellidoPPostulante,['class'=>'form-control','placeholder'=>'Apellido paterno estudiante','required']) !!}
					</div>
					<div class="form-group input-group">
						<span class="input-group-addon">
							<span class="glyphicon glyphicon-user"></span>
						</span>
						{!! Form::text('apellidoMPostulante',$proyecto->apellidoMPostulante,['class'=>'form-control','placeholder'=>'Apellido materno estudiante','required'])!!}
					</div>
					
					<div class="form-group input-group">
						<span class="input-group-addon">
							<span class="glyphicon glyphicon-user"></span>
						</span>
						{!! Form::text('objetivo',$proyecto->objetivo,['class'=>'form-control','placeholder'=>'objetivo']) !!}
					</div>
					<div class="form-group">
						{!! Form::label('modalidad','MODALIDAD DE TITULACION') !!}
						<div class="form-group input-group">
							<span class="input-group-addon">
								<span class="glyphicon glyphicon-education"></span>
							</span>
							{!! Form::select('modalidadTitulacion',[''=>'Seleccione una modalidad', 'Adscripcion'=> 'Adscripcion', 'Proyecto de grado'=>'Proyecto de grado','Trabajo dirijido'=> 'Trabajo dirijido', 'Proyecto de investigacion (tesis)'=>'Proyecto de investigacion (tesis)'],null,['class'=> 'form-control']) !!}
						</div>
					</div>
					<div class="form-group">
						{!! Form::label('carrera','Carrera') !!}
						<div class="form-group input-group">
							<span class="input-group-addon">
								<span class="glyphicon glyphicon-road"></span>
							</span>
							{!! Form::select('carrera',[''=>'Seleccione una carrera', 'Licenciatura en Ingenieria de Sistemas'=> 'Licenciatura en Ingenieria de Sistemas', 'Licenciatura en Ingenieria Informática'=>'Licenciatura en Ingenieria Informática'],null,['class'=> 'form-control']) !!}
						</div>
					</div>
					<div class="form-group">
						{!! Form::submit('Confirmar cambios',['class'=>'btn btn-primary', 'style'=>'width: 450px']) !!}
					</div>
					</div>

				{!! Form::close() !!}
			</div>
		</div>
	</div>
@endsection
@section('title')
	Admin-RegistroEstudiante
@endsection


@section('content-header')
	<section class="content-header">
      <h1>
        Zona de administracion
        <small>Registro Estudiante</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="prueba"><i class="fa fa-user-circle-o"></i> Admin</a></li>
        <li><a href="prueba">registro estudiante</a></li>
      </ol>
    </section>
@endsection