@extends('layouts.Admin')
@section('content')
<h3>Proyectos registrados</h3>
  <table class="table table-striped">
    <thead>
      <th>Nombre tutor</th>
      <th>Apellido paterno</th>
      <th>Nombre postulante</th>
      <th>Apellido postulante</th>
      <th>Modalidad de titulacion</th>
      <th>Carrera</th>
      <th>Periodo</th>
      <th>Editar/Eliminar </th>
    </thead>
    <tbody>
      @foreach($proyectos as $proyecto)
      <tr>
        <td>{{$proyecto->nombreTutor}}</td>
        <td>{{$proyecto->apellidoPTutor}}</td>
        <td>{{$proyecto->nombrePostulante}}</td>
        <td>{{$proyecto->apellidoPPostulante}}</td>
        <td>{{$proyecto->modalidadTitulacion}}</td>
        <td>{{$proyecto->carrera}}</td>
        <td>{{$proyecto->periodo}}</td>
        <td>
          {!! Form::open(['method'=>'DELETE','route' => ['proyecto.proyecto.destroy',$proyecto->id] ]) !!}
            <div class="form-group">
              <a href="{{route('proyecto.proyecto.edit',$proyecto->id)}}" class="btn btn-primary"><span class="glyphicon glyphicon-pencil"></a> 
              {!! Form::button('<span class="glyphicon glyphicon-remove"></span>',array('class'=>'btn btn-danger','type'=>'submit')) !!}
            </div>
          {!! Form::close()!!}
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>
  {!! $proyectos->render() !!} 
@endsection

@section('title')
	Admini-Inicio
@endsection


@section('content-header')
	<section class="content-header">
      <h1>
        Zona de administracion
        <small>Admin general</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="prueba"><i class="fa fa-user-circle-o"></i> Admin</a></li>
        <li><a href="prueba">inicio</a></li>
      </ol>
    </section>
@endsection