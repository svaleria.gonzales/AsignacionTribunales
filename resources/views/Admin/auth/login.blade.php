@extends('layouts.app')
@section('content')
	<div class="container">
		<div class="col-sm-10" style="width: 600px; margin-left: 250px; margin-top: 50px;">
			<div class="jumbotron">
				<div class="form-group" style="margin-top: -50px">
					<h1 style="margin-left: 110px;">Ingresar</h1>
				</div>

				<hr>
				{!! Form::open(['route' => 'admin.auth.login','method'=>'POST'],['style'=>'margin-left: 50px']) !!}
					<div class="form-group input-group">
						<span class="input-group-addon">
							<span class="glyphicon glyphicon-envelope"></span>
						</span>
						{!! Form::email('email',null,['class'=>'form-control','placeholder'=>'Ingrese su correo electronico','required']) !!}
					</div>
					<div class="form-group input-group">
						<span class="input-group-addon">
							<span class="glyphicon glyphicon-lock"></span>
						</span>
						{!! Form::password('password',['class'=>'form-control','placeholder'=>'Ingrese su clave','required']) !!}
					</div>
					<div class="form-group">
						{!! Form::submit('Ingresar',['class'=>'btn btn-primary', 'style'=>'width: 450px']) !!}
					</div>
				{!!Form::close()!!}
			</div>
		</div>
	</div>
@endsection
