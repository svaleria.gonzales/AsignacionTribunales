<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddProyectoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proyecto', function (Blueprint $table) {
            $table->increments('id');
            $table->string('tituloProyecto');
            $table->string('nombreTutor');
            $table->string('apellidoPTutor')->nullable();
            $table->string('apellidoMTutor')->nullable();
            $table->string('nombrePostulante');
            $table->string('apellidoPPostulante')->nullable();
            $table->string('apellidoMPostulante')->nullable();
            $table->string('objetivo')->nullable();
            $table->enum('modalidadTitulacion',['adscripcion','proyecto de grado','trabajo dirigido','proyecto de investigacion (tesis)']);
            $table->enum('carrera',['sistemas','informatica']);
            $table->date('fechaInicio')->nullable();
            $table->integer('periodo');
            $table->integer('area_id')->unsigned();
            $table->foreign('area_id')->references('id')->on('area')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('proyecto');
    }
}
