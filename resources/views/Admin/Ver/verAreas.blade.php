@extends('layouts.Admin')
@section('content')
<h3>Administradores</h3>
  <table class="table table-striped" >
    <thead>
      <th>Area</th>
      <th>Descripcion</th>
      <th>Acciones</th>
    </thead>
    <tbody>
      @foreach($areas as $area)
      <tr>
        <td>{{$area->nombre}}</td>
        <td>{{$area->descripcion}}</td>
        <td>
          {!! Form::open(['method'=>'DELETE','route' => ['area.area.destroy',$area->id] ]) !!}
            <div class="form-group">
              <a href="{{route('area.area.edit',$area->id)}}" class="btn btn-primary"><span class="glyphicon glyphicon-pencil"></a> 
              {!! Form::button('<span class="glyphicon glyphicon-remove"></span>',array('class'=>'btn btn-danger','type'=>'submit')) !!}
            </div>
          {!! Form::close()!!}
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>
  {!! $areas->render() !!} 
@endsection

@section('title')
	Admini-Inicio
@endsection


@section('content-header')
	<section class="content-header">
      <h1>
        Zona de administracion
        <small>Admin general</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="prueba"><i class="fa fa-user-circle-o"></i> Admin</a></li>
        <li><a href="prueba">inicio</a></li>
      </ol>
    </section>
@endsection