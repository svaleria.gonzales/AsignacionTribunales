<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Profesional</title>
    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="{{ asset('assets/css/bootstrap2.min.css') }}">
    <!-- Custom fonts for this template -->
    <link href="https://fonts.googleapis.com/css?family=Saira+Extra+Condensed:100,200,300,400,500,600,700,800,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdn.rawgit.com/konpa/devicon/df6431e323547add1b4cf45992913f15286456d3/devicon.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/simple-line-icons/2.4.1/css/simple-line-icons.css">
    <!-- Custom styles for this template -->
    <link rel="stylesheet" href="{{ asset('assets/css/resume.min.css') }}">
    <link rel="Shortcut Icon" href="{{asset('imagenes/docente.png')}}" type="image/x-icon" />
  </head>
  <body id="page-top"style=" background:url({{asset('imagenes/fondo2.jpg')}});background-size: cover; background-position: center; background-attachment: fixed; background-size: cover;" >
    @include('Docente.partials.sidebar')
    @yield('content')
    <!-- Bootstrap core JavaScript -->
    <script src="{{asset('assets/js/jquery.min.js')}}"></script>
    <script src="{{asset('assets/js/bootstrap.bundle.min.js')}}"></script>
    <!-- Plugin JavaScript -->
    <script src="{{asset('assets/js/jquery.easing.min.js')}}"></script>
    <!-- Custom scripts for this template -->
    <script src="{{asset('assets/js/resume.min.js')}}"></script>
  </body>
</html>
