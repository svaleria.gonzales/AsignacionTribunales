<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class AdminRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre' => 'min:4|max:120|required',
            'apellido' => 'min:5|max:120|required',
            'email'=> 'min:4|max:250|required|unique:administrador|unique:docente',
            'contrasenha' => 'min:8|max:20|required'
        ];
    }
}
