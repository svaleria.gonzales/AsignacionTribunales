@extends('layouts.Admin')
@section('content')
@if(count($errors)>0)
	<div class="alert alert-danger" role="alert">
		<ul>
			@foreach($errors->all() as $error)
				<li>{{$error}}</li>
			@endforeach
		</ul>
	</div>
@endif	
	<div class="container">
		<div class="col-sm-10" style="width: 600px; margin-left: 250px; margin-top: 50px;">
			<div class="jumbotron">
				<div class="form-group" style="margin-top: -50px">
					<h2 style="margin-left: 100px;">Registro Docente</h2>
					<img src="{{asset('imagenes/docente.png')}}" style="margin-left: 200px;">
				</div>

				<hr>
				{!! Form::open(['route' => 'docente.docente.store','method'=>'POST'],['style'=>'margin-left: 50px']) !!}

					<div class="form-group input-group">
						<span class="input-group-addon">
							<span class="glyphicon glyphicon-user"></span>
						</span>
						{!! Form::text('nombre',null,['class'=>'form-control','placeholder'=>'Nombre','required']) !!}
					</div>
					<div class="form-group input-group">
						<span class="input-group-addon">
							<span class="glyphicon glyphicon-user"></span>
						</span>
						{!! Form::text('apellidoP',null,['class'=>'form-control','placeholder'=>'Apellido paterno','required']) !!}
					</div>
					<div class="form-group input-group">
						<span class="input-group-addon">
							<span class="glyphicon glyphicon-user"></span>
						</span>
						{!! Form::text('apellidoM',null,['class'=>'form-control','placeholder'=>'Apellido materno','required']) !!}
					</div>
					<div class="form-group input-group">
						<span class="input-group-addon">
							<span class="glyphicon glyphicon-envelope"></span>
						</span>
						{!! Form::email('email',null,['class'=>'form-control','placeholder'=>'correo electronico','required']) !!}
					</div>
					<div class="form-group input-group">
						<span class="input-group-addon">
							<span class="glyphicon glyphicon-font"></span>
						</span>
						{!! Form::text('tituloDocente',null,['class'=>'form-control','placeholder'=>'Titulo docente','required']) !!}
					</div>
					<div class="form-group">
						{!! Form::label('type','Carga horaria') !!}
						<div class="form-group input-group">
							<span class="input-group-addon">
								<span class="glyphicon glyphicon-hourglass"></span>
							</span>
							{!! Form::select('cargaHoraria',['placeholder'=>'seleccione un tipo','Tiempo Parcial'=> 'Tiempo Parcial','Tiempo Completo'=> 'Tiempo Completo' ,'Invitado'=>'Invitado'],null,['class'=> 'form-control']) !!}
						</div>
					</div>
					<div class="form-group input-group">
						<span class="input-group-addon">
							<span class="glyphicon glyphicon-user"></span>
						</span>
						{!! Form::text('nombreCuenta',null,['class'=>'form-control','placeholder'=>'Nombre de la cuenta','required']) !!}
					</div>
					<div class="form-group input-group">
						<span class="input-group-addon">
							<span class="glyphicon glyphicon-phone"></span>
						</span>
						{!! Form::text('telefono',null,['class'=>'form-control','placeholder'=>'Telefono','required']) !!}
					</div>
					<div class="form-group input-group">
						<span class="input-group-addon">
							<span class="glyphicon glyphicon-map-marker"></span>
						</span>
						{!! Form::text('direccion',null,['class'=>'form-control','placeholder'=>'Direccion','required']) !!}
					</div>
					<div class="form-group input-group">
						<span class="input-group-addon">
							<span class="glyphicon glyphicon-user"></span>
						</span>
						{!! Form::text('perfil',null,['class'=>'form-control','placeholder'=>'Perfil','required']) !!}
					</div>
					<div class="form-group input-group">
						<span class="input-group-addon">
							<span class="glyphicon glyphicon-lock"></span>
						</span>
						{!! Form::password('contrasenha',['class'=>'form-control','placeholder'=>'Clave','required']) !!}
					</div>
					<div class="form-group input-group">
						<span class="input-group-addon">
							<span class="glyphicon glyphicon-user"></span>
						</span>
						{!! Form::text('carnet',null,['class'=>'form-control','placeholder'=>'carnet de identidad','required']) !!}
					</div>
					<div class="form-group input-group">
						<span class="input-group-addon">
							<span class="glyphicon glyphicon-user"></span>
						</span>
						{!! Form::text('codSis',null,['class'=>'form-control','placeholder'=>'Codigo SIS','required']) !!}
					</div>

					<div class="form-group">
						{!! Form::label('area_id','Area de competencia') !!}
						<div class="form-group input-group">
							<span class="input-group-addon">
								<span class="glyphicon glyphicon-book"></span>
							</span>
							{!! Form::select('area_id',$area,null,['class'=> 'form-control','placeholder'=>'seleccione un area','required'])!!}
							
						</div>
					</div>

					<div class="form-group">
						{!! Form::submit('Registrar',['class'=>'btn btn-primary', 'style'=>'width: 450px']) !!}
					</div>

				{!! Form::close()!!}
			</div>
		</div>
	</div>
@endsection

@section('title')
	Admin-RegistroDocente
@endsection


@section('content-header')
	<section class="content-header">
      <h1>
        Zona de administracion
        <small>Registro Docente</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="prueba"><i class="fa fa-user-circle-o"></i> Admin</a></li>
        <li><a href="prueba">Registro docente</a></li>
      </ol>
    </section>
@endsection


