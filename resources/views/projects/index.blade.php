@extends('layouts.app');

@section('content')
    <h1>Projects</h1>
    @if(count($projects) > 1)
    @foreach($projects as $project)
    <div class="well">
    <h3><a href="projects/{{$project->Project_Id}}">{{$project->Project_Title}}</a></h3>
    <small>Creado en fecha: {{$project->created_at}}</small>
    </div>
@endforeach
    @else
        <p>No projects fount </p>
    @endif
@endsection