<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Area extends Model
{
    protected $table = "area";
    protected $fillable = ['nombre','descripcion'];
    public function subAreas(){
    	return $this->hasMany('App\subArea');
    }
    public function docentes(){
    	return $this->hasMany('App\Docente');
    }
    public function proyecto(){
    	return $this->hasMany('App\Proyecto');
    }
}
