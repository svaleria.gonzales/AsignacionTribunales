<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Proyecto;
use Laracasts\Flash\Flash;
use App\Area;


class ProyectoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $proyecto = Proyecto::orderBy('id','ASC')->paginate(5);
        return view('Admin.Ver.verProyectos')->with('proyectos',$proyecto);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $area = Area::orderBy('nombre','ASC')->lists('nombre','id');
        return view('admin.Agregar.registroProyecto')->with('area',$area);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $proyecto = new Proyecto($request->all());
        $proyecto->save();
        return redirect()->route('proyecto.proyecto.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $proyecto = Proyecto::find($id);
        return view('Admin.Editar.editarProyecto')->with('proyecto',$proyecto);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $proyecto = Proyecto::find($id);
        $proyecto->nombrePostulante = $request->nombrePostulante;
        $proyecto->apellidoPPostulante = $request->apellidoPPostulante;
        $proyecto->apellidoMPostulante = $request->apellidoMPostulante;
        $proyecto->objetivo = $request->objetivo;
        $proyecto->modalidadTitulacion = $request->modalidadTitulacion;
        $proyecto->carrera = $request->carrera;
        $proyecto->save();
        return redirect()->route('proyecto.proyecto.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $proyecto = Proyecto::find($id);
        $proyecto->delete();
        return redirect()->route('proyecto.proyecto.index');
    }
}
