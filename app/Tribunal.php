<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tribunal extends Model
{
    protected $table = "tribunal";
    protected $fillable = ['proyecto_id'];
    public function docentes(){
    	return $this->belongsToMany('App\Docente');
    }
}
