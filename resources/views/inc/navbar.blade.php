<nav class="navbar navbar-inverse" style="background-color: #215F88">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                <span class="sr-only">Toggle Navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <!-- branding image -->
            <a class="navbar-brand" href="/">
                {{config('app.name', 'AsignacionTribunales')}}
            </a>
        </div>

        <div class="collapse navbar-collapse" id="app-navbar-collapse">
            <!-- Left Side Of Navbar -->
            <ul class="nav navbar-nav">
                &nbsp;
            </ul>
            <ul class="nav navbar-nav">
              <li><a href="/">Home</a></li>
              <li><a href="/AsignacionTribunales/public/about">About</a></li>
              <li><a href="/services">Services</a></li>
            </ul>

            <!-- Right Side Of Navbar -->
            <ul class="nav navbar-nav navbar-right">
                @yield('navDer');
                <!-- Authentication Links -->
            </ul>
        </div>
    </div>
</nav>