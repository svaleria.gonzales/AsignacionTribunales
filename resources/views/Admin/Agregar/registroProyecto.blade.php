@extends('layouts.Admin')
@section('content')
@if(count($errors)>0)
	<div class="alert alert-danger" role="alert">
		<ul>
			@foreach($errors->all() as $error)
				<li>{{$error}}</li>
			@endforeach
		</ul>
	</div>
@endif
	<div class="container">
		<div class="col-sm-10" style="width: 600px; margin-left: 250px; margin-top: 50px;">
			<div class="jumbotron">
				<div class="form-group" style="margin-top: -50px">
					<h2 style="margin-left: 90px;">Registro de Proyectos</h2>
					<img src="{{asset('imagenes/estudiante.png')}}" style="margin-left: 200px;">
				</div>

				<hr>

				{!! Form::open(['route' => 'proyecto.proyecto.store','method'=>'POST'],['style'=>'margin-left: 50px']) !!}
					<div class="form-group input-group">
						<span class="input-group-addon">
							<span class="glyphicon glyphicon-user"></span>
						</span>
						{!! Form::text('nombreTutor',null,['class'=>'form-control','placeholder'=>'Nombre del tutor','required']) !!}
					</div>
					<div class="form-group input-group">
						<span class="input-group-addon">
							<span class="glyphicon glyphicon-user"></span>
						</span>
						{!! Form::text('apellidoPTutor',null,['class'=>'form-control','placeholder'=>'Apellido paterno tutor','required']) !!}
					</div>
					<div class="form-group input-group">
						<span class="input-group-addon">
							<span class="glyphicon glyphicon-user"></span>
						</span>
						{!! Form::text('apellidoMTutor',null,['class'=>'form-control','placeholder'=>'Apellido materno tutor','required']) !!}
					</div>
					<div class="form-group input-group">
						<span class="input-group-addon">
							<span class="glyphicon glyphicon-user"></span>
						</span>
						{!! Form::text('nombrePostulante',null,['class'=>'form-control','placeholder'=>'Nombre del postulante','required']) !!}
					</div>
					<div class="form-group input-group">
						<span class="input-group-addon">
							<span class="glyphicon glyphicon-user"></span>
						</span>
						{!! Form::text('apellidoPPostulante',null,['class'=>'form-control','placeholder'=>'Apellido paterno estudiante','required']) !!}
					</div>
					<div class="form-group input-group">
						<span class="input-group-addon">
							<span class="glyphicon glyphicon-user"></span>
						</span>
						{!! Form::text('apellidoMPostulante',null,['class'=>'form-control','placeholder'=>'Apellido materno estudiante','required'])!!}
					</div>
					
					<div class="form-group input-group">
						<span class="input-group-addon">
							<span class="glyphicon glyphicon-user"></span>
						</span>
						{!! Form::text('objetivo',null,['class'=>'form-control','placeholder'=>'objetivo']) !!}
					</div>
					<div class="form-group">
						{!! Form::label('modalidad','MODALIDAD DE TITULACION') !!}
						<div class="form-group input-group">
							<span class="input-group-addon">
								<span class="glyphicon glyphicon-education"></span>
							</span>
							{!! Form::select('modalidadTitulacion',[''=>'Seleccione una modalidad', 'Adscripcion'=> 'Adscripcion', 'Proyecto de grado'=>'Proyecto de grado','Trabajo dirijido'=> 'Trabajo dirijido', 'Proyecto de investigacion (tesis)'=>'Proyecto de investigacion (tesis)'],null,['class'=> 'form-control']) !!}
						</div>
					</div>
					<div class="form-group">
						{!! Form::label('carrera','Carrera') !!}
						<div class="form-group input-group">
							<span class="input-group-addon">
								<span class="glyphicon glyphicon-road"></span>
							</span>
							{!! Form::select('carrera',[''=>'Seleccione una carrera', 'sistemas'=> 'Licenciatura en Ingenieria de Sistemas', 'informatica'=>'Licenciatura en Ingenieria Informática'],null,['class'=> 'form-control']) !!}
						</div>
					</div>
					<div class="form-group input-group">
						<span class="input-group-addon">
							<span class="glyphicon glyphicon-user"></span>
						</span>
						{!! Form::text('fechaRegistro',null,['class'=>'form-control','placeholder'=>'fecha de registro','required']) !!}
					</div>
					<div class="form-group input-group">
						<span class="input-group-addon">
							<span class="glyphicon glyphicon-user"></span>
						</span>
						{!! Form::text('periodo	',null,['class'=>'form-control','placeholder'=>'periodo','required']) !!}
					</div>
					<div class="form-group">
						{!! Form::label('area_id','Area proyecto') !!}
						<div class="form-group input-group">
							<span class="input-group-addon">
								<span class="glyphicon glyphicon-book"></span>
							</span>
							{!! Form::select('area_id',$area,null,['class'=> 'form-control','placeholder'=>'seleccione un area','required'])!!}
							
						</div>
					</div>

					<div class="form-group">
						{!! Form::submit('Registrar',['class'=>'btn btn-primary', 'style'=>'width: 450px']) !!}
					</div>

				{!! Form::close() !!}
			</div>
		</div>
	</div>
@endsection
@section('title')
	Admin-RegistroEstudiante
@endsection


@section('content-header')
	<section class="content-header">
      <h1>
        Zona de administracion
        <small>Registro Estudiante</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="prueba"><i class="fa fa-user-circle-o"></i> Admin</a></li>
        <li><a href="prueba">registro estudiante</a></li>
      </ol>
    </section>
@endsection