<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Designacion extends Model
{
    protected $table = "designacion";
    protected $fillable = ['aceptado','docenteTribunal_id'];
}
