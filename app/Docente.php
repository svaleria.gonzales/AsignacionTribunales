<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Docente extends Model
{
    protected $table = "docente";
    protected $fillable = ['nombre','apellidoP','apellidoM','email','tituloDocente','cargaHorario','nombreCuenta','telefono','direccion','perfil','contrasenha','carnet','codSis','area_id'];
    public function tribunales(){
    	return $this->belongsToMany('App\Tribunal');
    }
    public function area(){
    	return $this->belongTo('App\Area');
    }
}
