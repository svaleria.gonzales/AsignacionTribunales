<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Proyecto extends Model
{
    protected $table = "proyecto";
    protected $fillable = ['tituloProyecto','nombreTutor','apellidoPTutor','apellidoMTutor','nombrePostulante','apellidoPPostulante','apellidoMPostulante','objetivo','modalidadTitulacion','carrera','fechaInicio','periodo','area_id'];
    public function area(){
    	return $this->belongsTo('App\Area');
    }
    public function estadoProyecto(){
    	return $this->hasOne('App\EstadoProyecto');
    }

}
