@extends('layouts.Admin')
@section('content')
<h3>Administradores</h3>
  <table class="table table-striped" >
    <thead>
      <th>nombre</th>
      <th>apellido</th>
      <th>email</th>
      <th>Editar/Eliminar </th>
    </thead>
    <tbody>
      @foreach($administradores as $administrador)
      <tr>
        <td>{{$administrador->nombre}}</td>
        <td>{{$administrador->apellido}}</td>
        <td>{{$administrador->email}}</td>
        <td>
          {!! Form::open(['method'=>'DELETE','route' => ['admin.admin.destroy',$administrador->id] ]) !!}
            <div class="form-group">
              <a href="{{route('admin.admin.edit',$administrador->id)}}" class="btn btn-primary"><span class="glyphicon glyphicon-pencil"></a> 
              {!! Form::button('<span class="glyphicon glyphicon-remove"></span>',array('class'=>'btn btn-danger','type'=>'submit')) !!}
            </div>
          {!! Form::close()!!}
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>
  {!! $administradores->render() !!} 
@endsection

@section('title')
	Admini-Inicio
@endsection


@section('content-header')
	<section class="content-header">
      <h1>
        Zona de administracion
        <small>Admin general</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="prueba"><i class="fa fa-user-circle-o"></i> Admin</a></li>
        <li><a href="prueba">inicio</a></li>
      </ol>
    </section>
@endsection