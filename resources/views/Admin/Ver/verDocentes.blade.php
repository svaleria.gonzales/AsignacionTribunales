@extends('layouts.Admin')
@section('content')
<h3>Docentes registrados</h3>
  <table class="table table-striped">
    <thead>
      <th>Nombre</th>
      <th>Apellido</th>
      <th>Email</th>
      <th>Telefono</th>
      <th>Area de competencia</th>
      <th>Editar/Eliminar </th>
    </thead>
    <tbody>
      @foreach($docentes as $docente)
      <tr>
        <td>{{$docente->nombre}}</td>
        <td>{{$docente->apellidoP}}</td>
        <td>{{$docente->email}}</td>
        <td>{{$docente->telefono}}</td>
        <td>{{$docente->area_id}}</td>
        <td>
          {!! Form::open(['method'=>'DELETE','route' => ['docente.docente.destroy',$docente->id] ]) !!}
            <div class="form-group">
              <a href="{{route('docente.docente.edit',$docente->id)}}" class="btn btn-primary"><span class="glyphicon glyphicon-pencil"></a>
              {!! Form::button('<span class="glyphicon glyphicon-remove"></span>',array('class'=>'btn btn-danger','type'=>'submit')) !!}
            </div>
          {!! Form::close()!!}
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>
  {!! $docentes->render() !!} 
@endsection

@section('title')
	Admini-Inicio
@endsection


@section('content-header')
	<section class="content-header">
      <h1>
        Zona de administracion
        <small>Admin general</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="prueba"><i class="fa fa-user-circle-o"></i> Admin</a></li>
        <li><a href="prueba">inicio</a></li>
      </ol>
    </section>
@endsection