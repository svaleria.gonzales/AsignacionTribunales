<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Administrador;
use App\Http\Requests\AdminRequest;
use Excel;
use Illuminate\Support\Facades\Input;


class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $administrator = Administrador::orderBy('id','ASC')->paginate(5);
        return view('Admin.Ver.verAdministradores')->with('administradores',$administrator);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Admin.Agregar.registroAdm');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AdminRequest $request)
    {
        $administrator = new Administrador($request->all());
        //$administrator->password = bcrypt($request->password); para encriptar pass
        $administrator->save();
        return redirect()->route('admin.admin.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $administrator = Administrador::find($id);
        return view('Admin.Editar.editarAdmin')->with('admin',$administrator);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $administrator = Administrador::find($id);
        $administrator->nombre = $request->nombre;
        $administrator->apellido = $request->apellido;
        $administrator->email = $request->email;
        $administrator->save();
        return redirect()->route('admin.admin.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $administrator = Administrador::find($id);
        $administrator->delete();
        return redirect()->route('admin.admin.index');
    }

    public function import()
    {
        return view('Admin.Importar.ImportarDocente');

    }

    public function importcsv()
    {
        $file = Input::file('file');
        $file_name = $file->getClientOriginalName();
        $file->move('files', $file_name);
        $results = Excel::load('files/'.$file_name, function($reader){
            $reader->all();
        })->get();

        return view('prueba', ['prueba' => $results]);
    }
}
