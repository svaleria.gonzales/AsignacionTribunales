@extends('layouts.Admin')
@section('content')	
	<div class="container">
		<div class="col-sm-10" style="width: 600px; margin-left: 250px; margin-top: 50px;">
			<div class="jumbotron">
				<div class="form-group" style="margin-top: -50px">
					<h2 style="margin-left: 100px;">Editar Docente</h2>
					<img src="{{asset('imagenes/docente.png')}}" style="margin-left: 200px;">
				</div>

				<hr>
				{!! Form::open(['route' => ['docente.docente.update', $docente],'method'=>'PUT'],['style'=>'margin-left: 50px']) !!}

					<div class="form-group input-group">
						<span class="input-group-addon">
							<span class="glyphicon glyphicon-user"></span>
						</span>
						{!! Form::text('name',$docente->name,['class'=>'form-control','placeholder'=>'Ingrese su nombre','required']) !!}
					</div>
					<div class="form-group input-group">
						<span class="input-group-addon">
							<span class="glyphicon glyphicon-user"></span>
						</span>
						{!! Form::text('lastname',$docente->lastname,['class'=>'form-control','placeholder'=>'Ingrese su apellido','required']) !!}
					</div>
					<div class="form-group input-group">
						<span class="input-group-addon">
							<span class="glyphicon glyphicon-envelope"></span>
						</span>
						{!! Form::email('email',$docente->email,['class'=>'form-control','placeholder'=>'Ingrese su correo electronico','required']) !!}
					</div>

					<div class="form-group">
						{!! Form::label('type','Tipo Docente') !!}
						<div class="form-group input-group">
							<span class="input-group-addon">
								<span class="glyphicon glyphicon-hourglass"></span>
							</span>
							{!! Form::select('workload',[''=>'Seleccione un tipo', 'Regular'=> 'Regular', 'invitado'=>'invitado'],null,['class'=> 'form-control']) !!}
						</div>
					</div>

					<div class="form-group input-group">
						<span class="input-group-addon">
							<span class="glyphicon glyphicon-book"></span>
						</span>
						{!! Form::text('competition_area',$docente->competition_area,['class'=>'form-control','placeholder'=>'Area de competencia','required']) !!}
					</div>

					<div class="form-group">
						{!! Form::submit('Modificar',['class'=>'btn btn-primary', 'style'=>'width: 450px']) !!}
					</div>

				{!! Form::close()!!}
			</div>
		</div>
	</div>
@endsection

@section('title')
	Admin-RegistroDocente
@endsection


@section('content-header')
	<section class="content-header">
      <h1>
        Zona de administracion
        <small>Registro Docente</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="prueba"><i class="fa fa-user-circle-o"></i> Admin</a></li>
        <li><a href="prueba">Registro docente</a></li>
      </ol>
    </section>
@endsection


