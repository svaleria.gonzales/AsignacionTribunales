@extends('layouts.Admin')
@section('content')	
	<div class="container">
		<div class="col-sm-10" style="width: 600px; margin-left: 250px; margin-top: 50px;">
			<div class="jumbotron">
				<div class="form-group" style="margin-top: -50px">
					<h2 style="margin-left: 100px;">Importar Estudiantes</h2>
					<img src="imagenes/estudiante.png" style="margin-left: 200px;">
				</div>
				<div class="container">
					<form style="border: 4px solid #a1a1a1;margin-top: 15px;padding: 10px;" action="/AsignacionTribunales/public/postImport" class="form-horizontal" method="post" enctype="multipart/form-data">
					{{csrf_field()}}
					<center>
					<input type="file" name="docente">
					<input type="hidden" value="{{csrf_token()}}" name="_token"/>
					<br><br>
					<button type="submit" class="btn btn-primary">Importar desde CSV</button>
					</center>		
    				</form>
				</div>
			</div>
		</div>
	</div>
@endsection