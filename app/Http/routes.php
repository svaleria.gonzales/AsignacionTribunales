<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

//php artisan route:list      
Route::get('/', 'PagesController@index');


Route::get('/about', 'PagesController@about');
Route::get('/login', 'PagesController@login');
Route::get('/registroAdm', 'PagesController@registroAdm');
Route::get('/registroDoc', 'PagesController@registroDoc');
Route::get('/registroEst', 'PagesController@registroEst');
Route::get('verEstudiantesAdmin', 'PagesController@verEstudiantesAdmin');

Route::group(['prefix'=>'admin'],function(){
	Route::resource('admin','AdminController');
});
Route::group(['prefix'=>'docente'],function(){
	Route::resource('docente','ProfessorController');
});
Route::group(['prefix'=>'proyecto'],function(){
	Route::resource('proyecto','proyectoController');
});
Route::group(['prefix'=>'area'],function(){
	Route::resource('area','AreaController');
});
Route::get('admin/auth/login',[ 
	'uses'=> 'Auth\AuthController@getLogin',
	'as' => 'admin.auth.login'
]);
Route::post('admin/auth/login',[ 
	'uses'=> 'Auth\AuthController@postLogin',
	'as' => 'admin.auth.login'
]);
Route::get('admin/auth/logout',[ 
	'uses'=> 'Auth\AuthController@getLogout',
	'as' => 'admin.auth.logout'
]);
Route::get('inicio-admin',function(){
	return view('Admin.index');
});
Route::get('inicio-docente',function(){
	return view('Docente.index');
});

Route::get('importarDocente', [
	'uses' => 'AdminController@import',
	'as' => 'importarDocente'
]);

Route::post('prueba',[ 
	'uses'=> 'AdminController@importcsv',
	'as' => 'prueba'
]);
//Route::post('prueba', 'AdminController@importcsv');
Route::get('getImport', 'ExcelController@getImport');
Route::post('postImport', 'ExcelController@postImport');