<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/> 
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
            
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
            
        <title>{{ config('app.name', 'Laravel') }}</title>
            
        <!-- Styles -->
        <link href="{{ asset('assets/css/bootstrap.css') }}" rel="stylesheet">
        <link rel="Shortcut Icon" href="{{asset('imagenes/user.png')}}" type="image/x-icon" />
        @include('inc.navbar')
    </head>
    <body background="{{asset('imagenes/fondo2.jpg')}}" style="background-size: cover; background-position: center; background-attachment: fixed">
        @yield('content')
    </body>
</html>