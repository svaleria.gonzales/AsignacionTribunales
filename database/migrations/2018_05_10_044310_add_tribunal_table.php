<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTribunalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tribunal', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('proyecto_id')->unsigned();
            $table->foreign('proyecto_id')->references('id')->on('proyecto')->onDelete('cascade');
            $table->timestamps();
        });
        Schema::create('docente_tribunal', function(Blueprint $table){
            $table->increments('id');
            $table->integer('tribunal_id')->unsigned();
            $table->integer('docente_id')->unsigned();
            $table->foreign('tribunal_id')->references('id')->on('tribunal');
            $table->foreign('docente_id')->references('id')->on('docente');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tribunal');
    }
}
