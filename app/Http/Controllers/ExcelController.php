<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Docente;
use App\Area;
use App\subArea;
use App\Proyecto;
use Input;
use Excel;
use DB;

class ExcelController extends Controller
{
    public function getImport()
    {
        return view('excel.importarDocente');

    }

    public function postImport(Request $request)
    {
        

        $archivo = $request->file('docente');
        $nombreArchivo = $archivo->getClientOriginalName();
//IMPORTAR AREAS
                Excel::selectSheetsByIndex(3)->load($archivo, function($hoja){
                $hoja->each(function($fila){
                //echo $fila;
                $subarea_lista = subArea::where("nombre", "=", $fila->nombre)->first();
                if(count($subarea_lista) == 0){
                    if($fila->codigo_subarea){
                        $id = $fila->codigo_subarea;
                        $area_1 = Area::where("id", "=", $fila->codigo_subarea)->first();
                 
                        if(count($area_1) == 1){
                         
                            $subarea = new subArea;
                            $subarea->id = $fila->codigo;
                            $subarea->nombre = $fila->nombre;
                            $subarea->descripcion = $fila->descripcion;
                            $subarea->area_id = $fila->codigo_subarea;
                            $subarea->save();
                        }else
                        {
                            echo "VOLVER A ENVIAR EL ARCHIVO ya que algunas subareas tienen relacion con areas que no habian sido creadas<br>";
                        }
                      
                        
                    }else{
                        $area_lista = Area::where("nombre", "=", $fila->nombre)->first();
                        if(count($area_lista) == 0){
                            
                            $area = new Area;
                            $area->id = $fila->codigo;
                            $area->nombre = $fila->nombre;
                            $area->descripcion = $fila->descripcion;
                            $area->save();
                        }
                    }
                }
               
            });
            echo "Areas y subareas importadas<br>";
        });

//IMPORTAR PROYECTO

        Excel::selectSheetsByIndex(0)->load($archivo, function($hoja){
            $hoja->each(function($fila){
                //echo $fila;
                $lista_proyectos = Proyecto::where("tituloProyecto", "=", $fila->titulo_proyecto_final)->first();
               
                if(count($lista_proyectos) == 0 && $fila->titulo_proyecto_final)
                {
                    $proyecto = new Proyecto;
                    $proyecto->tituloProyecto = $fila->titulo_proyecto_final;
                    $proyecto->nombreTutor = $fila->nombre_tutor;
                    $proyecto->apellidoPTutor = $fila->apellido_paterno_tutor;
                    $proyecto->apellidoMTutor = $fila->apellido_materno_tutor;
                    $proyecto->nombrePostulante = $fila->nombre_postulante;
                    $proyecto->apellidoPPostulante = $fila->apellido_paterno_postulante;
                    $proyecto->apellidoMPostulante = $fila->apellido_materno_postulante;
                    $proyecto->objetivo = $fila->objetivo_general;
                    $proyecto->carrera = "informatica";
                    $proyecto->fechaInicio = $fila->fecha_de_registro;
                    if($fila->modalidad_titulacion == "Adscripci\u00f3n"){$proyecto->modalidadTitulacion = "adscripcion";}
                    if($fila->modalidad_titulacion == "Proyecto de Grado"){$proyecto->modalidadTitulacion = "proyecto de grado";}
                    if($fila->modalidad_titulacion == "Trabajo Dirigido"){$proyecto->modalidadTitulacion = "trabajo dirigido";}
                    if($fila->modalidad_titulacion == "Proyecto de Investigaci\u00f3n (Tesis)"){$proyecto->modalidadTitulacion = "proyecto de investigacion (tesis)";}
                    $proyecto->periodo = $fila->periodo;
                    $proyecto->area_id = 1;
                    $proyecto->save();
                }
                
            });
            echo "Proyectos de informatica importados<br>";
        });



        Excel::selectSheetsByIndex(1)->load($archivo, function($hoja){
            $hoja->each(function($fila){
                //echo $fila;
                $lista_proyectos = Proyecto::where("tituloProyecto", "=", $fila->titulo_proyecto_final)->first();
               
                if(count($lista_proyectos) == 0 && $fila->titulo_proyecto_final)
                {
                    $proyecto = new Proyecto;
                    $proyecto->tituloProyecto = $fila->titulo_proyecto_final;
                    $proyecto->nombreTutor = $fila->nombre_tutor;
                    $proyecto->apellidoPTutor = $fila->apellido_paterno_tutor;
                    $proyecto->apellidoMTutor = $fila->apellido_materno_tutor;
                    $proyecto->nombrePostulante = $fila->nombre_postulante;
                    $proyecto->apellidoPPostulante = $fila->apellido_paterno_postulante;
                    $proyecto->apellidoMPostulante = $fila->apellido_materno_postulante;
                    $proyecto->objetivo = $fila->objetivo_general;
                    $proyecto->carrera = "sistemas";
                    $proyecto->fechaInicio = $fila->fecha_de_registro;
                    if($fila->modalidad_titulacion == "Adscripci\u00f3n"){$proyecto->modalidadTitulacion = "adscripcion";}
                    if($fila->modalidad_titulacion == "Proyecto de Grado"){$proyecto->modalidadTitulacion = "proyecto de grado";}
                    if($fila->modalidad_titulacion == "Trabajo Dirigido"){$proyecto->modalidadTitulacion = "trabajo dirigido";}
                    if($fila->modalidad_titulacion == "Proyecto de Investigaci\u00f3n (Tesis)"){$proyecto->modalidadTitulacion = "proyecto de investigacion (tesis)";}
                    $proyecto->periodo = $fila->periodo;
                    $proyecto->area_id = 1;
                    $proyecto->save();
                }
                
            });
            echo "Proyectos de sistemas importados<br>";
        });

//IMPORTAR DOCENTE
        Excel::selectSheetsByIndex(2)->load($archivo, function($hoja){
            $hoja->each(function($fila){
                //echo $fila;
                $docente_emails = Docente::where("email", "=", $fila->correo)->first();
                
                if(count($docente_emails) == 0){
                    $docente = new Docente;
                    $docente->nombre = $fila->nombre;
                    $docente->apellidoP = $fila->apellido_paterno;
                    $docente->apellidoM = $fila->apellido_materno;
                    $docente->email = $fila->correo;
                    $docente->tituloDocente = $fila->titulo_docente;
                    $docente->cargaHoraria = $fila->carga_horaria;
                    $docente->nombreCuenta = $fila->nombre_cuenta;
                    $docente->telefono = $fila->telefono;
                    $docente->direccion = $fila->direccion;
                    $docente->perfil = $fila->perfil;
                    $docente->contrasenha = $fila->contrasena_cuenta;
                    $docente->carnet = $fila->ci;
                    $docente->codSis = $fila->cod_sis;
                    $docente->area_id = 1;
                    $docente->save();
                }

            });
            echo "Docentes importados<br>";
        });

        // $docente = Docente::orderBy('id','ASC')->paginate(5);
        // return view('Admin.Ver.verDocentes')->with('docentes',$docente);
    }
}
