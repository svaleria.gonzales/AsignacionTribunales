<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDesignacionTribunalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('designacion', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('aceptado');
            $table->integer('docenteTribunal_id')->unsigned();
            $table->foreign('docenteTribunal_id')->references('id')->on('docente_tribunal')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('designacion');
    }
}
