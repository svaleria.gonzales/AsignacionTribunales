<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class StudentRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'min:4|max:120|required',
            'lastname' => 'min:5|max:120|required',
            'email'=> 'min:4|max:250|required|unique:administrator|unique:student|unique:professor',
            'password' => 'min:8|max:20|required'
        ];
    }
}
