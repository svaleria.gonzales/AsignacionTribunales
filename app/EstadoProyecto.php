<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EstadoProyecto extends Model
{
    protected $table = "estadoproyecto";
    protected $fillable = ['habilitado','proyecto_id'];
    public function proyecto(){
    	return $this->belongsTo('App\Proyecto');
    }
}
