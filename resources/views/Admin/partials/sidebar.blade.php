
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="{{ asset('imagenes/adm.png') }}" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>Alexander Pierce</p>
        </div>
      </div>

      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">ADMINISTRACION</li>
        <li class="treeview">
          <a href="">
            <i class="fa fa-address-book-o"></i> 
            <span>Ver</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{route('proyecto.proyecto.index')}}"><i class="fa fa-user-o"></i> Proyecto</a></li>
            <li><a href="{{route('docente.docente.index')}}"><i class="fa fa-user-circle-o"></i> Docente</a></li>
            <li><a href="{{route('admin.admin.index')}}"><i class="fa fa-user-secret"></i> Administrador</a></li>
            <li><a href="../../index2.html"><i class="fa fa-book"></i> Proyectos</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-user-plus"></i>
            <span>Agregar</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
              <li class="treeview">
                <a href="#">
                  <i class="fa fa-user-o"></i>
                  <span>Areas</span>
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
                </a>
                <ul class="treeview-menu">
                  <li><a href=""><i class="fa fa-book"></i> Por lotes</a></li>
                  <li><a href="{{route('area.area.create')}}"><i class="fa fa-user-o"></i> Un area</a></li>
                </ul>

              <li class="treeview">
                <a href="#">
                  <i class="fa fa-user-o"></i>
                  <span>Proyecto</span>
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
                </a>
                <ul class="treeview-menu">
                  <li><a href=""><i class="fa fa-book"></i> Por lotes</a></li>
                  <li><a href="{{route('proyecto.proyecto.create')}}"><i class="fa fa-user-o"></i> Un proyecto</a></li>
                </ul>
              </li>
              <li class="treeview">
                <a href="#">
                  <i class="fa fa-user-circle-o"></i>
                  <span>Docente</span>
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
                </a>
                <ul class="treeview-menu">
                  <li><a href="{{route('importarDocente')}}"><i class="fa fa-book"></i> Por lotes</a></li>
                  <li><a href="{{route('admin.admin.create')}}"><i class="fa fa-user-circle-o"></i> Un docente</a></li>
                </ul>
              </li>
            <li><a href="{{route('admin.admin.create')}}"><i class="fa fa-user-secret"></i> Administrador</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-puzzle-piece"></i>
            <span>Asignar</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="../../index2.html"><i class="fa fa-book"></i> Proyectos</a></li> 
          </ul>
        </li>
    </section>
    <!-- /.sidebar -->